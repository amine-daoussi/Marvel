import { prop } from 'ramda';
export const getListCharacters = prop('listCharacters');
export const getCharacter = prop('character');
