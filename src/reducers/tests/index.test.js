import reducers from '..';

import { LOAD_ALL_CHARACTERS, LOAD_CHARACTER } from '../../actions';

jest.mock('axios', () => ({
  get: () => Promise.resolve({ data: { id: 1011334, name: '3-D Man', description: '' } }),
}));

describe('reducers', () => {
  it('should load all characters', () => {
    const payload = { listCharacters: { id: 1011334, name: '3-D Man', description: '' } };
    const start = {
      type: LOAD_ALL_CHARACTERS,
      ...payload,
    };
    expect(reducers({}, start)).toEqual({ ...payload });
  });
  it('should load a character', () => {
    const payload = { listCharacters: {} };
    const start = {
      type: LOAD_ALL_CHARACTERS,
      ...payload,
    };
    expect(reducers({}, start)).toEqual({ ...payload });
  });
});
