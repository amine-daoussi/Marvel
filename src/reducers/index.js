import { LOAD_ALL_CHARACTERS, LOAD_CHARACTER } from '../actions';

export default (state, action) => {
  switch (action.type) {
    case LOAD_ALL_CHARACTERS:
      return {
        ...state,
        listCharacters: action.listCharacters,
      };
    case LOAD_CHARACTER:
      return {
        ...state,
        character: action.character,
      };
    default:
      return state;
  }
};
