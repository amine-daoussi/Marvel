import axios from 'axios';

import { MARVEL_URL, MARVEL_URL_BY_ID } from '../apiData';

export const loadMarvelCharacters = () => axios.get(MARVEL_URL).then(res => res.data.data.results);

export const loadMarvelCharacterById = id => axios.get(MARVEL_URL_BY_ID(id)).then(res => res.data.data.results[0]);
