import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import theme from '../../theme';

import configureStore from '../../store';

const store = configureStore();

const AllProviders = ({ children }) => (
  <ThemeProvider theme={theme}>
    <BrowserRouter>
      <CssBaseline />
      <Provider store={store}>{children}</Provider>
    </BrowserRouter>
  </ThemeProvider>
);

AllProviders.propTypes = {
  children: PropTypes.element,
};

export const customRender = (ui, options) => render(ui, { wrapper: AllProviders, options });
export * from '@testing-library/react';
export { customRender as render };
