import React, { useEffect } from 'react';
import { Grid, Typography, CardMedia, Button } from '@material-ui/core';
import { useParams, useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import LoadingMarvel from '../LoadingMarvel';
import { getCharacter } from '../../selectors';
import { loadCharacterById } from '../../actions';

const useStyles = makeStyles(theme => ({
  container: { backgroundColor: 'black', color: 'white', minHeight: '100vh' },
  pageImage: {
    [theme.breakpoints.down('sm')]: { width: '200px' },
    width: '35vw',
  },
  pageDescription: {
    [theme.breakpoints.down('sm')]: { width: '350px', margin: '0' },
    width: '30vw',
    fontSize: '3vh',
    marginLeft: '20px',
  },
  title: { fontSize: '4vh', textAlign: 'center' },
  informations: {
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      alignItems: 'center',
      textAlign: 'justify',
      margin: '5px',
    },
    width: 'max-content',
  },
  card: {
    width: 'max-content',
  },
  link: {
    color: 'white',
  },
}));

const CharacterPage = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const character = useSelector(getCharacter);
  const { id } = useParams();
  const history = useHistory();
  useEffect(() => dispatch(loadCharacterById(id)), []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleBackLink = () => {
    history.goBack();
  };

  if (!character) return <LoadingMarvel />;
  return (
    <Grid className={classes.container}>
      <Button className={classes.link} onClick={handleBackLink}>
        ---- Home Page
      </Button>
      :
      <Grid container justify="center" alignItems="flex-Start" className={classes.informations} wrap="wrap">
        <Grid item container direction="column" justify="center" className={classes.card}>
          <CardMedia component="img" src={`${character.thumbnail.path}.jpg`} className={classes.pageImage} />
          <Typography className={classes.title}>{character.name}</Typography>
        </Grid>
        <Grid item>
          <Typography className={classes.pageDescription}>{character.description}</Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default CharacterPage;
