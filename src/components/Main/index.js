import React, { useEffect } from 'react';
import { Grid, CardMedia } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';

import { getListCharacters } from '../../selectors';
import { loadListCharacters } from '../../actions';
import CharacterCard from '../CharacterCard';
import LoadingMarvel from '../LoadingMarvel';

const useStyles = makeStyles(() => ({
  container: { backgroundColor: 'black', color: 'white' },
  image: { width: '25vw', textAlign: 'center' },
}));
const Main = () => {
  const dispatch = useDispatch();
  const listCharacters = useSelector(getListCharacters);
  const classes = useStyles();

  useEffect(() => {
    dispatch(loadListCharacters());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Grid className={classes.container}>
      {!listCharacters ? (
        <LoadingMarvel />
      ) : (
        <Grid container justify="center">
          <CardMedia
            component="img"
            src="https://www.hdwallpapersfreedownload.com/uploads/large/super-heroes/marvel-avengers-hd-background-wallpaper.jpg"
            className={classes.image}
          />
          <Grid container justify="center" alignItems="center" wrap="wrap">
            {listCharacters.map(el => (
              <CharacterCard title={el.name} image={`${el.thumbnail.path}.jpg`} id={el.id} key={el.id} />
            ))}
          </Grid>
        </Grid>
      )}
    </Grid>
  );
};

export default Main;
