import React from 'react';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Grid, Typography, CardMedia } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  card: {
    border: '1px solid black',
    margin: '10px',
    backgroundColor: 'black',
  },
  cardTitle: { textAlign: 'center', color: 'white' },
  cardImage: {
    height: '350px',
    width: '300px',
  },
}));
const CharacterCard = ({ title, image, id }) => {
  const classes = useStyles();
  return (
    <Grid item className={classes.card}>
      <Link to={`/${id}`}>
        <CardMedia component="img" src={image} className={classes.cardImage} />
      </Link>
      <Typography className={classes.cardTitle}>{title}</Typography>
    </Grid>
  );
};
CharacterCard.propTypes = {
  title: propTypes.string,
  image: propTypes.string,
  id: propTypes.number,
};

export default CharacterCard;
