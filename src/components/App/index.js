import React from 'react';

import { Route, Switch } from 'react-router-dom';

import Main from '../Main';
import CharacterPage from '../CharacterPage';

const App = () => {
  return (
    <Switch>
      <Route exact path="/:id" component={CharacterPage} />
      <Route path="/" component={Main} />
    </Switch>
  );
};

export default App;
