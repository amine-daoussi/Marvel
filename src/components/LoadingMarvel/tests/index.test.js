import React from 'react';
import { render } from '../../../utils/testing';
import Component from '..';

describe('components | LoadingMarvel ', () => {
  it('should render component', () => {
    const { container } = render(<Component />);
    expect(container).toMatchSnapshot();
  });
});
