import React from 'react';
import { Grid, CardMedia } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles(() => ({
  loading: { width: '100px', fontSize: '100px', color: '#79090f' },
  container: { height: '100vh', backgroundColor: 'black' },
  image: { height: 'auto', width: '40vw' },
}));
const LoadingMarvel = () => {
  const classes = useStyles();
  return (
    <Grid container alignItems="center" justify="center" direction="column" className={classes.container}>
      <CardMedia
        component="img"
        src="https://mlpnk72yciwc.i.optimole.com/cqhiHA-rNTEul5O/w:600/h:323/q:90/http://www.bleedingcool.com/wp-content/uploads/2014/03/marvel.jpg"
        className={classes.image}
      />
      <Grid>
        <Icon className={`fas fa-circle-notch fa-spin ${classes.loading}`} />
      </Grid>
    </Grid>
  );
};

export default LoadingMarvel;
