import * as marvel from '../services';

export const LOAD_ALL_CHARACTERS = 'LOAD_ALL_CHARACTERS';
export const LOAD_CHARACTER = 'LOAD_CHARACTER';
export const loadListCharacters = () => async dispatch => {
  const listCharacters = await marvel.loadMarvelCharacters();
  setTimeout(
    () =>
      dispatch({
        type: LOAD_ALL_CHARACTERS,
        listCharacters,
      }),
    1200,
  );
};

export const loadCharacterById = id => async dispatch => {
  const character = await marvel.loadMarvelCharacterById(id);

  dispatch({
    type: LOAD_CHARACTER,
    character,
  });
};
