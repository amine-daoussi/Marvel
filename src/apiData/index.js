const crypto = require('crypto');
const API_PUBLIC_KEY = '298bab46381a6daaaee19aa5c8cafea5';
const API_PRIVATE_KEY = 'b0223681fced28de0fe97e6b9cd091dd36a5b71d';
const ts = Math.round(+new Date() / 1000);

const concatenatedString = `${ts}${API_PRIVATE_KEY}${API_PUBLIC_KEY}`;
const hash = crypto
  .createHash('md5')
  .update(concatenatedString)
  .digest('hex');

export const MARVEL_URL = `http://gateway.marvel.com:80/v1/public/characters?ts=${ts}&apikey=${API_PUBLIC_KEY}&hash=${hash}`;
export const MARVEL_URL_BY_ID = id =>
  `http://gateway.marvel.com:80/v1/public/characters/${id}?ts=${ts}&apikey=${API_PUBLIC_KEY}&hash=${hash}`;
